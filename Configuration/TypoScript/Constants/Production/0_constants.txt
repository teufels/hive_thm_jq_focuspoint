#############################
## CONSTANTS FOCUSPOINT.JS ##
#############################

plugin.tx_teufels_thm_jq_focuspoint {
	settings {
        production {
            includePath {
                public = EXT:teufels_thm_jq_focuspoint/Resources/Public/
                private = EXT:teufels_thm_jq_focuspoint/Resources/Private/
                frontend {
                    public = typo3conf/ext/teufels_thm_jq_focuspoint/Resources/Public/
                }

            }
        }
    }
}