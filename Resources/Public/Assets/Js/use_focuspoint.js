/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Andreas Hafner, Dominik Hilser, Georg Kathan, Hendrik Krüger, Timo Bittner - teufels GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 **/
var teufels_thm_jq_focuspoint__interval = setInterval(function () {

    if (typeof jQuery == 'undefined') {
    }  else {

        if ( typeof teufels_cfg_typoscript__windowLoad == 'undefined') {
        } else {

            if (typeof teufels_cfg_typoscript__windowLoad == "boolean" && teufels_cfg_typoscript__windowLoad) {

                clearInterval(teufels_thm_jq_focuspoint__interval);

                if (teufels_cfg_typoscript_sStage == "prototype" || teufels_cfg_typoscript_sStage == "development") {
                    console.info('jQuery focuspoint loaded');
                }

                $('.focuspoint').focusPoint({
                    throttleDuration: 250, //re-focus images at most once every 100ms.
                    reCalcOnWindowResize: true
                });
            }

        }

      }

}, 500);